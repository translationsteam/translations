﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TranslationModels;
using TranslationInterfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TranslationWebApi.Controllers
{
    [Route("translation/[controller]")]
    public class CurrencyController : Controller
    {
        public CurrencyController(ICurrencyTranslator currencyTranslator)
        {
            this.currencyTranslator = currencyTranslator;
        }
        private ICurrencyTranslator currencyTranslator;
       
        // POST api/values
        [HttpPost("[action]")]
        public IActionResult Translate([FromBody]CurrencyModel model)
        {
            var currencyModel = currencyTranslator.Translate(model);
            return Json(currencyModel);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Translations.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {   
            TempData["applicationUrl"] = Translations.Program.Configuration["Services:Translations:ApplicationUrl"]; 
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}

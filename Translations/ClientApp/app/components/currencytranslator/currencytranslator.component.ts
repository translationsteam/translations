﻿import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

@Component({
    selector: 'currencytranslator',
    templateUrl: './currencytranslator.component.html'
})
export class CurrencyTranslatorComponent implements OnInit{
    
    public message: CurrencyTranslator;
    public translate: CurrencyTranslator;
    private http: Http;
    private webApiUrl: string;
    private controllerAddress: string;
    private headers: Headers; 
    private options: RequestOptions;

    constructor(http: Http, @Inject('WEBAPI_URL') webApiUrl: string) {
        this.http = http;
        this.webApiUrl = webApiUrl;

    }
    ngOnInit(): void {

        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
        this.controllerAddress = "translation/Currency/";
        this.message = new CurrencyTranslator();
        this.translate = new CurrencyTranslator();
    }
    onTranslateEvent() {
        let self = this;
        if (self.message == null || self.message.message == null) {
            return;
        }
        let model = JSON.stringify(this.message);
        self.http.post(this.webApiUrl + this.controllerAddress + 'Translate', model, self.options)
            .subscribe(
                translate => {
                    self.translate = JSON.parse(translate.text());
                   // translate => this.translate = translate
                },
                err => {
                    console.log("Error occured");
                });
    }
    onClearEvent() {
        this.message = new CurrencyTranslator();
        this.translate = new CurrencyTranslator();
    }

}
class CurrencyTranslator {
    message: string 
    constructor() {
    }
}

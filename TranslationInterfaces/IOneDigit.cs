﻿using System;
using System.Collections.Generic;
using System.Text;
/// <summary>
/// Namespace for TranslationInterfaces
/// </summary>
namespace TranslationInterfaces
{
    /// <summary>
    /// IOneDigit Interface
    /// </summary> 
    public interface IOneDigit: IDigit
    {
    }
}

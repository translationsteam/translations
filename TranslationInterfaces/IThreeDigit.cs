﻿using System;
using System.Collections.Generic;
using System.Text;
/// <summary>
/// Namespace for TranslationInterfaces
/// </summary>
namespace TranslationInterfaces
{
     /// <summary>
    /// IThreeDigit Interface
    /// </summary> 
    public interface IThreeDigit:IDigit
    {
    }
}

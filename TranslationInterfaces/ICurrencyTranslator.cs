﻿using System;
using System.Collections.Generic;
using System.Text;
using TranslationModels;
/// <summary>
/// Namespace for TranslationInterfaces
/// </summary>
namespace TranslationInterfaces
{
     /// <summary>
    /// ICurrencyTranslator Interface
    /// </summary> 
    public interface ICurrencyTranslator
    {
         CurrencyModel Translate(CurrencyModel model);
    }
}

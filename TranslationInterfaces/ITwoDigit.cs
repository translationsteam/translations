﻿using System;
using System.Collections.Generic;
using System.Text;
/// <summary>
/// Namespace for TranslationInterfaces
/// </summary>
namespace TranslationInterfaces
{
    /// <summary>
    /// ITwoDigit Interface
    /// </summary> 
    public interface ITwoDigit : IDigit
    {
    }
}

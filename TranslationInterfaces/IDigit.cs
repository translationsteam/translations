﻿using System;
using System.Collections.Generic;
using System.Text;
/// <summary>
/// Namespace for TranslationInterfaces
/// </summary>
namespace TranslationInterfaces
{
     /// <summary>
    /// IDigit Interface
    /// </summary>
   public interface IDigit
    {
        string GetDigitsTranslation(string value);
    }
}

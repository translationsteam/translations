﻿using System;
using System.Collections.Generic;
using System.Text;
/// <summary>
/// Namespace for TranslationInterfaces
/// </summary>
namespace TranslationInterfaces
{
     /// <summary>
    /// IDigitBuilder Interface
    /// </summary> 
    public interface IDigitBuilder
    {
        List<IDigit> BuildDigitProcessors();
        IDigit GetDigitProcessor<T>() where T:IDigit;
    }
}

using CurrencyTranslationService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using TranslationInterfaces;
using TranslationModels;

namespace CurrencyTranslatorServiceTests
{
    [TestClass]
    public class CurrencyTranslatorTests: ICurrencyTranslator
    {
        Mock<IOneDigit> oneDigitMOCK;
        Mock<ITwoDigit> twoDigitMOCK;
        Mock<IThreeDigit> threeDigitMOCK;
        Mock<IFourDigit> fourDigitMOCK;

        Mock<IDigitBuilder> digitBuilderMOCK;
        ICurrencyTranslator currencyTranslator;
        
        [TestInitialize()]
        public void Initialize()
        {
            oneDigitMOCK = new Mock<IOneDigit>();
            twoDigitMOCK = new Mock<ITwoDigit>();
            threeDigitMOCK = new Mock<IThreeDigit>();
            fourDigitMOCK = new Mock<IFourDigit>();

            digitBuilderMOCK = new Mock<IDigitBuilder>();
            digitBuilderMOCK.Setup(s => s.GetDigitProcessor<IOneDigit>()).Returns(oneDigitMOCK.Object);
            digitBuilderMOCK.Setup(s => s.GetDigitProcessor<ITwoDigit>()).Returns(twoDigitMOCK.Object);

            var mockedDigitList = new List<IDigit>
            {
                 fourDigitMOCK.Object,
                threeDigitMOCK.Object,
                twoDigitMOCK.Object,
                oneDigitMOCK.Object
            };
            digitBuilderMOCK.Setup(s => s.BuildDigitProcessors()).Returns(mockedDigitList);

            currencyTranslator = new CurrencyTranslator(digitBuilderMOCK.Object);
        }
        [TestMethod]
        public void GetDigitsTranslationForEmptyStringReturnsEmptyString()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John"
            };
            var expectedValue = model;

            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }

        [TestMethod]
        public void GetDigitsTranslationFor1CentReturnsOneCent()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John .01"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John ONE CENT"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("01")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("01")).Returns("");


            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }
        [TestMethod]
        public void GetDigitsTranslationFor11CentReturnsElevenCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John .11"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John ELEVEN CENTS"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("11")).Returns("");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("11")).Returns("Eleven");


            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }

        [TestMethod]
        public void GetDigitsTranslationFor21CentReturnsTwentyOneCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John .21"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John TWENTY-ONE CENTS"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("Twenty-");

            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }

        [TestMethod]
        public void GetDigitsTranslationFor121CentReturnsOneDollarTwentyOneCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John 1.21"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John ONE DOLLAR AND TWENTY-ONE CENTS"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("Twenty-");

            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("1")).Returns("One");


            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }
        [TestMethod]
        public void GetDigitsTranslationForNegative121CentReturnsOneDollarTwentyOneCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John -1.21"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John NEGATIVE ONE DOLLAR AND TWENTY-ONE CENTS"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("Twenty-");

            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("1")).Returns("One");


            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }
        [TestMethod]
        public void GetDigitsTranslationFor2121CentReturnsTwentyOneDollarTwentyOneCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John 21.21"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John TWENTY-ONE DOLLARS AND TWENTY-ONE CENTS"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("Twenty-");


            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }
        [TestMethod]
        public void GetDigitsTranslationFor32121CentReturnsThreeHundredAndTwentyOneDollarTwentyOneCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John 321.21"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John THREE HUNDRED AND TWENTY-ONE DOLLARS AND TWENTY-ONE CENTS"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("Twenty-");

            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("321")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("321")).Returns("Twenty-");
            threeDigitMOCK.Setup(s => s.GetDigitsTranslation("321")).Returns("Three Hundred and ");

            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }

        [TestMethod]
        public void GetDigitsTranslationFor432121CentReturnsFourThousandThreeHundredAndTwentyOneDollarTwentyOneCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John 4321.21"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John FOUR THOUSAND THREE HUNDRED AND TWENTY-ONE DOLLARS AND TWENTY-ONE CENTS"
            };
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("21")).Returns("Twenty-");

            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("4321")).Returns("One");
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("4321")).Returns("Twenty-");
            threeDigitMOCK.Setup(s => s.GetDigitsTranslation("4321")).Returns("Three Hundred and ");
            fourDigitMOCK.Setup(s => s.GetDigitsTranslation("4321")).Returns("Four Thousand ");

            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message);
        }
        public CurrencyModel Translate(CurrencyModel model)
        {
            return currencyTranslator.Translate(model);
        }
    }
}

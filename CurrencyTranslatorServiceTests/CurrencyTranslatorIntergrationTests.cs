﻿using CurrencyTranslationService;
using DigitTranslations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TranslationInterfaces;
using TranslationModels;

namespace CurrencyTranslatorServiceTests
{
    //NOTE: An integration tests can be performed on this clas as there are no external dependencies,like a database


    [TestClass]
    public class CurrencyTranslatorIntergrationTests : ICurrencyTranslator
    {
        IOneDigit oneDigit;
        ITwoDigit twoDigit;
        IThreeDigit threeDigit;
        IFourDigit fourDigit;

        
        IDigitBuilder digitBuilder;
        ICurrencyTranslator currencyTranslator;

        [TestInitialize()]
        public void Initialize()
        {
            oneDigit = new OneDigit();
            twoDigit = new TwoDigit();
            threeDigit = new ThreeDigit(oneDigit, twoDigit);
            fourDigit = new FourDigit(oneDigit);

           
            digitBuilder = new DigitBuilder(oneDigit, twoDigit,threeDigit, fourDigit);

            currencyTranslator = new CurrencyTranslator(digitBuilder);
        }
        [TestMethod]
        public void GetDigitsTranslationFor432121CentReturnsFourThousandThreeHundredAndTwentyOneDollarTwentyOneCents()
        {
            // ARRANGE
            CurrencyModel model = new CurrencyModel
            {
                Message = "John 4321.21"
            };
            var expectedValue = new CurrencyModel
            {
                Message = "John Four Thousand Three Hundred and Twenty-One dollars and Twenty-One cents".ToUpperInvariant()
            };
           

            // ACT
            var actualValue = Translate(model);

            // ASSERT		
            Assert.AreEqual(expectedValue.Message, actualValue.Message,true,"");
        }
        public CurrencyModel Translate(CurrencyModel model)
        {
            return currencyTranslator.Translate(model);
        }
    }
}

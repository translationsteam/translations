﻿using DigitTranslations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TranslationInterfaces;

namespace DigitTranslationTests
{
    [TestClass]
    public class TwoDigitTests : ITwoDigit
    {
        Mock<IOneDigit> oneDigitMOCK;
        private ITwoDigit twoDigit { get; set; }
        [TestInitialize()]
        public void Initialize()
        {
            oneDigitMOCK = new Mock<IOneDigit>();
            twoDigit = new TwoDigit();
        }
        [TestMethod]
        public void GetDigitsTranslationForEmptyStringReturnsEmptyString()
        {
            // ARRANGE
            var value = "";
            var expectedValue = string.Empty;

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_10_ReturnsTen()
        {
            // ARRANGE
            var value = "10";
            var expectedValue = "TEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_20_ReturnsTwenty()
        {
            // ARRANGE
            var value = "20";
            var expectedValue = "TWENTY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_30_ReturnsThirty()
        {
            // ARRANGE
            var value = "30";
            var expectedValue = "THIRTY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_40_ReturnsForty()
        {
            // ARRANGE
            var value = "40";
            var expectedValue = "FORTY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_50_ReturnsFifty()
        {
            // ARRANGE
            var value = "50";
            var expectedValue = "FIFTY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_60_ReturnsSixty()
        {
            // ARRANGE
            var value = "60";
            var expectedValue = "SIXTY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_70_ReturnsSeventy()
        {
            // ARRANGE
            var value = "70";
            var expectedValue = "SEVENTY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_80_ReturnsEighty()
        {
            // ARRANGE
            var value = "80";
            var expectedValue = "EIGHTY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_90_ReturnsNinety()
        {
            // ARRANGE
            var value = "90";
            var expectedValue = "NINETY ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetDigitsTranslationFor_11_ReturnsEleven()
        {
            // ARRANGE
            var value = "11";
            var expectedValue = "ELEVEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_12_ReturnsTwelve()
        {
            // ARRANGE
            var value = "12";
            var expectedValue = "TWELVE ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_13_ReturnsThirteen()
        {
            // ARRANGE
            var value = "13";
            var expectedValue = "THIRTEEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_14_ReturnsFourteen()
        {
            // ARRANGE
            var value = "14";
            var expectedValue = "FOURTEEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_15_ReturnsFifteen()
        {
            // ARRANGE
            var value = "15";
            var expectedValue = "FIFTEEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_16_ReturnsSixteen()
        {
            // ARRANGE
            var value = "16";
            var expectedValue = "SIXTEEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_18_ReturnsEighteen()
        {
            // ARRANGE
            var value = "18";
            var expectedValue = "EIGHTEEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_19_ReturnsNinteen()
        {
            // ARRANGE
            var value = "19";
            var expectedValue = "NINETEEN ";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }





        [TestMethod]
        public void GetDigitsTranslationFor_22_ReturnsTwenty()
        {
            // ARRANGE
            var value = "22";
            var expectedValue = "TWENTY-";
            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_33_ReturnsThirty()
        {
            // ARRANGE
            var value = "33";
            var expectedValue = "THIRTY-";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_44_ReturnsForty()
        {
            // ARRANGE
            var value = "44";
            var expectedValue = "FORTY-";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_55_ReturnsFifty()
        {
            // ARRANGE
            var value = "55";
            var expectedValue = "FIFTY-";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_66_ReturnsSixty()
        {
            // ARRANGE
            var value = "66";
            var expectedValue = "SIXTY-";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_77_ReturnsSeventy()
        {
            // ARRANGE
            var value = "77";
            var expectedValue = "SEVENTY-";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_88_ReturnsEighty()
        {
            // ARRANGE
            var value = "88";
            var expectedValue = "EIGHTY-";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_99_ReturnsNinety()
        {
            // ARRANGE
            var value = "99";
            var expectedValue = "NINETY-";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        public string GetDigitsTranslation(string value)
        {
            return twoDigit.GetDigitsTranslation(value);
        }
    }
}

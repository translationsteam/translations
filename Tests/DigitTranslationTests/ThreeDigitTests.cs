﻿using DigitTranslations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TranslationInterfaces;

namespace DigitTranslationTests
{
    [TestClass]
    public class ThreeDigitTests : IThreeDigit
    {
        Mock<IOneDigit> oneDigitMOCK;
        Mock<ITwoDigit> twoDigitMOCK;
        private IThreeDigit threeDigit { get; set; }
        [TestInitialize()]
        public void Initialize()
        {
            oneDigitMOCK = new Mock<IOneDigit>();
            twoDigitMOCK = new Mock<ITwoDigit>();
            threeDigit = new ThreeDigit(oneDigitMOCK.Object, twoDigitMOCK.Object);
        }
        [TestMethod]
        public void GetDigitsTranslationForEmptyStringReturnsEmptyString()
        {
            // ARRANGE
            var value = "";
            var expectedValue = string.Empty;

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetDigitsTranslationFor099ReturnsEmptyString()
        {
            // ARRANGE
            var value = "099";
            var expectedValue = string.Empty;
            twoDigitMOCK.Setup(s => s.GetDigitsTranslation("99")).Returns(String.Empty);

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }


        [TestMethod]
        public void GetDigitsTranslationFor100ReturnsOneHundred()
        {
            // ARRANGE
            var value = "100";
            var expectedValue = "ONE HUNDRED ";
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("1")).Returns("One");

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor120ReturnsOneHundredAnd()
        {
            // ARRANGE
            var value = "120";
            var expectedValue = "ONE HUNDRED AND ";
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("1")).Returns("One");

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        public string GetDigitsTranslation(string value)
        {
            return threeDigit.GetDigitsTranslation(value);
        }
    }
}

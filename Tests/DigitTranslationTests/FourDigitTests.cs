﻿using DigitTranslations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TranslationInterfaces;

namespace DigitTranslationTests
{
    [TestClass]
    public class FourDigitTests:IFourDigit
    {
        Mock<IOneDigit> oneDigitMOCK;
        private IFourDigit fourDigit { get; set; }
        [TestInitialize()]
        public void Initialize()
        {
            oneDigitMOCK = new Mock<IOneDigit>();
            fourDigit = new FourDigit(oneDigitMOCK.Object);
        }
        [TestMethod]
        public void GetDigitsTranslationForEmptyStringReturnsEmptyString()
        {
            // ARRANGE
            var value = "";
            var expectedValue = string.Empty;

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetDigitsTranslationFor1000ReturnsThousand()
        {
            // ARRANGE
            var value = "1000";
            var expectedValue = "ONE THOUSAND ";
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("1")).Returns("One");

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor1200ReturnsThousand()
        {
            // ARRANGE
            var value = "1200";
            var expectedValue = "ONE THOUSAND ";
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("1")).Returns("One");

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor2000ReturnsThousand()
        {
            // ARRANGE
            var value = "2000";
            var expectedValue = "TWO THOUSAND ";
            oneDigitMOCK.Setup(s => s.GetDigitsTranslation("2")).Returns("Two");

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        public string GetDigitsTranslation(string value)
        {
            return fourDigit.GetDigitsTranslation(value);
        }
    }
}

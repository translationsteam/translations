using Microsoft.VisualStudio.TestTools.UnitTesting;
using DigitTranslations;
using TranslationInterfaces;

namespace DigitTranslationTests
{
    [TestClass]
    public class OneDigitTests: IOneDigit
    {
        private IOneDigit oneDigit { get; set; }
        [TestInitialize()]
        public void Initialize()
        {
            oneDigit = new OneDigit();
        }
        [TestMethod]
        public void GetDigitsTranslationForEmptyStringReturnsEmptyString()
        {
            // ARRANGE
            var value = "";
            var expectedValue = string.Empty;          

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetDigitsTranslationFor_0_ReturnsEmptyString()
        {
            // ARRANGE
            var value = "0";
            var expectedValue = string.Empty;

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_10_ReturnsEmptyString()
        {
            // ARRANGE
            var value = "10";
            var expectedValue = string.Empty;

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_1_ReturnsOne()
        {
            // ARRANGE
            var value = "1";
            var expectedValue = "ONE";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_2_ReturnsTwo()
        {
            // ARRANGE
            var value = "2";
            var expectedValue = "TWO";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_3_ReturnsThree()
        {
            // ARRANGE
            var value = "3";
            var expectedValue = "THREE";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_4_ReturnsFour()
        {
            // ARRANGE
            var value = "4";
            var expectedValue = "FOUR";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_5_ReturnsFive()
        {
            // ARRANGE
            var value = "5";
            var expectedValue = "FIVE";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_6_ReturnsSix()
        {
            // ARRANGE
            var value = "6";
            var expectedValue = "SIX";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetDigitsTranslationFor_7_ReturnsSeven()
        {
            // ARRANGE
            var value = "7";
            var expectedValue = "SEVEN";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetDigitsTranslationFor_8_ReturnsEight()
        {
            // ARRANGE
            var value = "8";
            var expectedValue = "EIGHT";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        [TestMethod]
        public void GetDigitsTranslationFor_9_ReturnsNine()
        {
            // ARRANGE
            var value = "9";
            var expectedValue = "NINE";

            // ACT
            var actualValue = GetDigitsTranslation(value);

            // ASSERT		
            Assert.AreEqual(expectedValue, actualValue);
        }
        public string GetDigitsTranslation(string value)
        {
            return oneDigit.GetDigitsTranslation(value);
        }
    }
}

﻿using System;
using TranslationInterfaces;
/// <summary>
/// Namespace for DigitTranslations
/// </summary> 
namespace DigitTranslations
{
     /// <summary>
    /// OneDigit class
    /// Inherits Interface <see cref="IOneDigit"/>  
    /// </summary> 
    public class OneDigit : IOneDigit
    {
        public string GetDigitsTranslation(string value)
        {
            int number = 0;
            if(String.IsNullOrEmpty(value) || value.Length < 1)
            {
                return String.Empty;
            }
            int.TryParse(value.Substring(value.Length - 1, 1), out number);
            if(number == 0)
            {
                return String.Empty;
            }
            string result = String.Empty;
            switch(number)
            {
                case 1:
                    result = "One";
                    break;
                case 2:
                    result = "Two";
                    break;
                case 3:
                    result = "Three";
                    break;
                case 4:
                    result = "Four";
                    break;
                case 5:
                    result = "Five";
                    break;
                case 6:
                    result = "Six";
                    break;
                case 7:
                    result = "Seven";
                    break;
                case 8:
                    result = "Eight";
                    break;
                case 9:
                    result = "Nine";
                    break;
            }
            return result.ToUpperInvariant();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using TranslationInterfaces;
/// <summary>
/// Namespace for DigitTranslations
/// </summary>
namespace DigitTranslations
{
     /// <summary>
    /// FourDigit class
    /// Inherits Interface <see cref="IFourDigit"/>  
    /// </summary> 
    public class FourDigit : IFourDigit
    {
        public FourDigit(IOneDigit oneDigit)
        {
            this.oneDigit = oneDigit;
        }
        private IOneDigit oneDigit;
        public string GetDigitsTranslation(string value)
        {
            int number = 0;
            if(String.IsNullOrEmpty(value) || value.Length < 4)
            {
                return String.Empty;
            }
            int.TryParse(value.Substring(value.Length - 4, 4), out number);
            if(number == 0)
            {
                return String.Empty;
            }
            int subNumber = 0;
            string numberAsStr = number.ToString();
            // get left most digit
            int.TryParse(numberAsStr.Substring(0, 1), out subNumber);
            if(subNumber == 0)
            {
                return String.Empty;
            }
            return String.Concat(oneDigit.GetDigitsTranslation(subNumber.ToString()), " Thousand ").ToUpperInvariant();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using TranslationInterfaces;
/// <summary>
/// Namespace for DigitTranslations
/// </summary> 
namespace DigitTranslations
{
    /// <summary>
    /// TwoDigit class
    /// Inherits Interface <see cref="ITwoDigit"/>  
    /// </summary> 
    public class TwoDigit : ITwoDigit
    {
        
        public string GetDigitsTranslation(string value)
        {
            int number = 0;
            if(String.IsNullOrEmpty(value) || value.Length < 2)
            {
                return String.Empty;
            }
            int.TryParse(value.Substring(value.Length - 2, 2), out number);
            if(number == 0)
            {
                return String.Empty;
            }
            string result = String.Empty;
            result = parseZeroSecondDigit(number, result);
            if(!String.IsNullOrEmpty(result))
            {
                return result.ToUpperInvariant();
            }
            if(number > 10 && number < 20)
            {
                result = between10and20(number);
            }
        

            result = parseNonZeroSecondDigit(number, result);
            return result.ToUpperInvariant();
        }

        private static string parseNonZeroSecondDigit(int number, string result)
        {
            if(number > 20 && number < 30)
            {
                result = "Twenty-";
            }
            if(number > 30 && number < 40)
            {
                result = "Thirty-";
            }
            if(number > 40 && number < 50)
            {
                result = "Forty-";
            }
            if(number > 50 && number < 60)
            {
                result = "Fifty-";
            }
            if(number > 60 && number < 70)
            {
                result = "Sixty-";
            }
            if(number > 70 && number < 80)
            {
                result = "Seventy-";
            }
            if(number > 80 && number < 90)
            {
                result = "Eighty-";
            }
            if(number > 90 && number <= 99)
            {
                result = "Ninety-";
            }

            return result;
        }

        private  string parseZeroSecondDigit(int number, string result)
        {
            switch(number)
            {
                case 10:
                    result = "Ten ";
                    break;
                case 20:
                    result = "Twenty ";
                    break;
                case 30:
                    result = "Thirty ";
                    break;
                case 40:
                    result = "Forty ";
                    break;
                case 50:
                    result = "Fifty ";
                    break;
                case 60:
                    result = "Sixty ";
                    break;
                case 70:
                    result = "Seventy ";
                    break;
                case 80:
                    result = "Eighty ";
                    break;
                case 90:
                    result = "Ninety ";
                    break;
            }

            return result;
        }

        private string between10and20(int number)
        {
            String result = String.Empty;
            switch(number)
            {
                case 11:
                    result = "Eleven ";
                    break;
                case 12:
                    result = "Twelve ";
                    break;
            }
            int teenNumber = 0;
            string numberAsStr = number.ToString();
            // get second digit
            int.TryParse(numberAsStr.Substring(1, 1), out teenNumber); 
            if(teenNumber == 0)
            {
                return String.Empty;
            }
            switch(teenNumber)
            {
                case 3:
                    result = "Thirteen ";
                    break;
                case 4:
                    result = "Fourteen ";
                    break;
                case 5:
                    result = "Fifteen ";
                    break;
                case 6:
                    result = "Sixteen ";
                    break;
                case 7:
                    result = "Seventeen ";
                    break;
                case 8:
                    result = "Eighteen ";
                    break;
                case 9:
                    result = "Nineteen ";
                    break;
            }
            return result;
        }
    }
}

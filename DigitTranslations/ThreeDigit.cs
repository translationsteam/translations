﻿using System;
using System.Collections.Generic;
using System.Text;
using TranslationInterfaces;
/// <summary>
/// Namespace for DigitTranslations
/// </summary> 
namespace DigitTranslations
{
    /// <summary>
    /// ThreeDigit class
    /// Inherits Interface <see cref="IThreeDigit"/>  
    /// </summary>
    public class ThreeDigit : IThreeDigit
    {
        public ThreeDigit(IOneDigit oneDigit, ITwoDigit twoDigit)
        {
            this.oneDigit = oneDigit;
            this.twoDigit = twoDigit;
        }
        private IOneDigit oneDigit;
        private ITwoDigit twoDigit;
        public string GetDigitsTranslation(string value)
        {
            int number = 0;
            if(String.IsNullOrEmpty(value) || value.Length < 3)
            {
                return String.Empty;
            }
            int.TryParse(value.Substring(value.Length - 3, 3), out number);
            if(number == 0 )
            {
                return String.Empty;
            }
            if(number <= 99)
            {
                return twoDigit.GetDigitsTranslation(number.ToString());
            }
            int subNumber = 0;
            string numberAsStr = number.ToString();
            // get left most digit
            int.TryParse(numberAsStr.Substring(0, 1), out subNumber);
            if(subNumber == 0)
            {
                return String.Empty;
            }
            string result =  String.Concat(oneDigit.GetDigitsTranslation(subNumber.ToString()), " Hundred ");
            int twoRightDigits = 0;
            // get right most 2 digits
            int.TryParse(numberAsStr.Substring(numberAsStr.Length - 2, 2), out twoRightDigits);
            if(twoRightDigits > 0)
            {
                result = String.Concat(result, "and ");
            }
            return result.ToUpperInvariant();
        }
    }
}

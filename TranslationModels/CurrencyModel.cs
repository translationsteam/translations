﻿using System;
/// <summary>
/// Namespace for TranslationModels
/// </summary>
namespace TranslationModels
{
    /// <summary>
    /// CurrencyModel class
    /// </summary>
    public class CurrencyModel
    {
        public String Message { get; set; }
    }
}

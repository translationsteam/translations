﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using TranslationInterfaces;
using TranslationModels;
/// <summary>
/// Namespace for CurrencyTranslationService
/// </summary>
namespace CurrencyTranslationService
{
     /// <summary>
    /// CurrencyTranslator class
    /// Inherits Interface <see cref="ICurrencyTranslator"/>  
    /// </summary>
    public class CurrencyTranslator : ICurrencyTranslator
    {
        /// <summary>
        /// CurrencyTranslator constructor
        /// </summary> 
        /// <param name="builder"><see cref="DigitBuilder"/> class of Interface <see cref="IDigitBuilder"</param>  
        public CurrencyTranslator(IDigitBuilder builder)
        {
            this.builder = builder;
        }
        private IDigitBuilder builder;
        private string currencyPattern = @"^\-?\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)*(.[0-9][0-9])?$";
        private string currencySymbol = "$";
        private string negativeSymbol = "-";
        /// <summary>
        /// Translate method that manages the translation of numbers words 
        /// </summary> 
        /// <param name="model"><see cref="CurrencyModel"/> model class</param> 
        /// <returns>model of <see cref="CurrencyModel"/></returns> 
        public CurrencyModel Translate(CurrencyModel model)
        {
            if(String.IsNullOrEmpty(model.Message))
            {
                model.Message = String.Empty;
                return model;
            }
           
            string[] msg = model.Message.Split(" ");
            string[] resultMsg = new string[msg.Length];
            int pos = 0;
            for(int index=0; index<msg.Length; index++)
            {
                parseMessageParts(msg, resultMsg, pos);
            }



            CurrencyModel resultModel = new CurrencyModel();
            resultModel.Message = string.Join(" ",resultMsg);
            return resultModel;
        }
         /// <summary>
        /// parseMessageParts is recursive method that parses each word in the message
        /// </summary> 
        /// <param name="msg">array of words that were the message</param>          
        /// <param name="resultMsg">empty array that is the same size,ie elements as msg</param> 
        /// <param name="pos">position of next word to parse in the msg array</param> 
        private void parseMessageParts(string[] msg, string[] resultMsg, int pos)
        {
            StringBuilder sbrNumber = new StringBuilder();
            string value = msg[pos];
            Regex rgx = new Regex(currencyPattern);
            MatchCollection matches = rgx.Matches(value);
            if(matches.Count > 0)
            {
                if(value.Contains("-"))
                {
                    sbrNumber.Append("NEGATIVE ");
                    value = value.Replace(negativeSymbol, "");
                }
                value = value.Replace(currencySymbol, "");
                string dollarValue = getDollarValue(value);
                string centValue = getCentsValue(value);

                sbrNumber.Append(dollarValue);
                if(!String.IsNullOrEmpty(dollarValue))
                {
                    sbrNumber.Append(" and ");
                    sbrNumber.Append(centValue);
                }
                else
                {
                    sbrNumber.Append(centValue);
                }
                resultMsg[pos] = sbrNumber.ToString().ToUpperInvariant();
            }
            else
            {
                //put the vvalue in the same position in the result msg
                resultMsg[pos] = value;
            }
            pos++;
            if(pos < msg.Length)
            {
                parseMessageParts(msg, resultMsg, pos); ;
            }
        }
         /// <summary>
        /// getDollarValue method that gets the left side of the decimal point of the number
        /// </summary> 
        /// <param name="value">the word to examine for a number</param>
        /// <returns>string being the left side value</returns> 
        private string getDollarValue(String value)
        {
            var leftSideValue = getLeftSideAsValue(value);
            if(String.IsNullOrEmpty(leftSideValue))
            {
                return String.Empty;
            }
            StringBuilder sbrDollar = new StringBuilder();
            var processors = builder.BuildDigitProcessors();
            foreach(var processor in processors)
            {
                var part = processor.GetDigitsTranslation(leftSideValue);
                if(!String.IsNullOrEmpty(part))
                {
                    sbrDollar.Append(part);
                }
            }
            if(sbrDollar.ToString().Equals("One", StringComparison.InvariantCultureIgnoreCase))
            {
                sbrDollar.Append(" dollar");
            }
            else
            {
                sbrDollar.Append(" dollars");
            }
            return sbrDollar.ToString();
        }
         /// <summary>
        /// getCentsValue method that gets the right side of the decimal point of the number
        /// </summary> 
        /// <param name="value">the word to examine for a number</param>        
        /// <returns>string being the right side value</returns> 
        private string getCentsValue(String value)
        {
            var rightSideValue = getRightSideAsValue(value);
            if(String.IsNullOrEmpty(rightSideValue))
            {
                return String.Empty;
            }
            StringBuilder sbrCents = new StringBuilder();
            var processorTwo = builder.GetDigitProcessor<ITwoDigit>();
            string twoVal = processorTwo.GetDigitsTranslation(rightSideValue);
            sbrCents.Append(twoVal);
            var processorOne = builder.GetDigitProcessor<IOneDigit>();
            string oneVal = processorOne.GetDigitsTranslation(rightSideValue);
            sbrCents.Append(oneVal);
            
            if(sbrCents.ToString().Equals("One", StringComparison.InvariantCultureIgnoreCase))
            {
                sbrCents.Append(" cent");
            }
            else
            {
                sbrCents.Append(" cents");
            }
            return sbrCents.ToString();
        }

         /// <summary>
        /// getLeftSideAsValue method that turns the left side of the word to a raw number
        /// </summary> 
        /// <param name="value">the word to examine for a number</param>               
        /// <returns>string being a raw number or empty string</returns> 
        private string getLeftSideAsValue(string value)
        {
            string noDollarSign = value.Replace("$","");
            string[] valueArr = noDollarSign.Split(".");
            if(valueArr.Length == 0 )
            {
                return String.Empty;
            }
            return valueArr[0];
        }
         /// <summary>
        /// getRightSideAsValue method that turns the right side of the  word to a raw number
        /// </summary> 
        /// <param name="value">the word to examine for a number</param>               
        /// <returns>string being a raw number or empty string</returns> 
        private string getRightSideAsValue(string value)
        {
            string[] valueArr = value.Split(".");
            if(valueArr.Length <2)
            {
                return String.Empty;
            } 
            if(valueArr[1].Trim().Length == 1)
            {
                return String.Concat(valueArr[1], "0");
            }
            return valueArr[1];
        }
    }
}

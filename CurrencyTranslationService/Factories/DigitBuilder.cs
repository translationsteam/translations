﻿using DigitTranslations;
using System;
using System.Collections.Generic;
using System.Text;
using TranslationInterfaces;
/// <summary>
/// Namespace for CurrencyTranslationService
/// </summary> 
namespace CurrencyTranslationService
{
    /// <summary>
    /// DigitBuilder class
    /// Inherits Interface <see cref="IDigitBuilder"/>  
    /// </summary> 
    public class DigitBuilder : IDigitBuilder
    {
        /// <summary>
        /// DigitBuilder constructor
        /// </summary> 
        /// <param name="oneDigit"><see cref="OneDigit"/> class of Interface <see cref="IOneDigit"/></param>  
        /// <param name="twoDigit"><see cref="TwoDigit"/> class of Interface <see cref="ITwoDigit"/></param> 
        /// <param name="threeDigit"><see cref="ThreeDigit"/> class of Interface <see cref="IThreeDigit"/></param> 
        /// <param name="fourDigit"><see cref="FourDigit"/> class of Interface <see cref="IFourDigit"/></param> 
       public DigitBuilder(IOneDigit oneDigit, ITwoDigit twoDigit, IThreeDigit threeDigit, IFourDigit fourDigit)
        {
            this.oneDigit = oneDigit;
            this.twoDigit = twoDigit;
            this.threeDigit = threeDigit;
            this.fourDigit = fourDigit;
    }

        private IOneDigit oneDigit;
        private ITwoDigit twoDigit;
        private IThreeDigit threeDigit;
        private IFourDigit fourDigit;
        /// <summary>
        /// BuildDigitProcessors method that creates an ordered list of digit processors
        /// </summary> 
        /// <returns>list of <see cref="IDigit"/></returns> 
        public List<IDigit> BuildDigitProcessors()
        {
            List<IDigit> digitProcessors = new List<IDigit>();
            //Digit classes must added in order of largest to smallest 
            digitProcessors.Add(fourDigit);
            digitProcessors.Add(threeDigit);
            digitProcessors.Add(twoDigit);
            digitProcessors.Add(oneDigit);

            return digitProcessors;
        }
        /// <summary>
        /// GetDigitProcessor method that creates an ordered list of digit processors
        /// </summary> 
        /// <typeparam name="T">Generic interface of type <see cref="IDigit"/></typeparam> 
        /// <returns>list of <see cref="IDigit"/></returns> 
        public IDigit GetDigitProcessor<T>() where T : IDigit
        {
            IDigit result = null;
            Type interfaceType = typeof(T);
            Type oneDigitType = typeof(IOneDigit);
            Type twoDigitType = typeof(ITwoDigit);
            switch(interfaceType.Name)
            {
                case "IOneDigit":
                    result = oneDigit; 
                    break;
                case "ITwoDigit":
                    result = twoDigit;
                    break;
            }
           return result;
        }
    }
}
